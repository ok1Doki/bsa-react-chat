import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter } from "react-router-dom";
import "typeface-roboto/index.css";
import App from "./App";
import registerServiceWorker from "./registerServiceWorker";
import "./index.css";

const rootEl = document.getElementById("root");

const render = Component => {
  ReactDOM.render(
    <BrowserRouter>
      <Component />
    </BrowserRouter>,
    rootEl
  );
};

render(App);

if (module.hot) {
  module.hot.accept("./App", () => {
    render(App);
  });
}

registerServiceWorker();
