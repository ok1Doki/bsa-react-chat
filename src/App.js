import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import ChatPage from "./components/ChatPage";

const styles = () => ({
  root: {
    alignItems: "center",
    display: "flex",
    height: "100%",
    margin: "0 auto",
    maxWidth: "900px",
    padding: "20px 0",
    position: "relative",
    width: "100%"
  }
});

const App = ({ classes }) => (
  <div className={classes.root}>
    <ChatPage
      activeUser={{
        firstName: "Nazar",
        lastName: "Panasiuk"
      }}
      messages={[]}
    />
  </div>
);

App.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired
};

export default withStyles(styles)(App);
