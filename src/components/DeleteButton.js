import * as React from "react";
import { IconButton } from "material-ui";
import { Delete } from "material-ui-icons";
import { withStyles } from "material-ui/styles";
import PropTypes from "prop-types";

const styles = () => ({
  button: {
    color: "grey",
    fontSize: "16px",
    margin: "4px",
    width: "auto"
  }
});

class DeleteButton extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    messageId: PropTypes.objectOf(PropTypes.string).isRequired,
    deleteMessage: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  deleteMessage() {
    this.props.deleteMessage(this.props.messageId);
  }

  render() {
    const { classes } = this.props;
    return (
      <div>
        <IconButton
          className={classes.button}
          onClick={this.deleteMessage}
          component="span"
        >
          <Delete />
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)(DeleteButton);
