/* eslint no-underscore-dangle: 0 */
import React from "react";
import PropTypes from "prop-types";
import { Scrollbars } from "react-custom-scrollbars";
import { withStyles } from "material-ui/styles";
import { animateScroll } from "react-scroll";
import Typography from "material-ui/Typography";
import ChatMessage from "./ChatMessage";

const styles = theme => ({
  messagesWrapper: {
    height: "100%",
    width: "100%"
  },
  paper: {
    padding: theme.spacing.unit * 3
  }
});

class ChatMessageList extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    messages: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        avatar: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        createdAt: PropTypes.string.isRequired,
        editedAt: PropTypes.string.isRequired
      })
    ).isRequired,
    activeUser: PropTypes.shape({
      firstName: PropTypes.string,
      lastName: PropTypes.string,
      username: PropTypes.string
    }).isRequired,
    deleteMessage: PropTypes.func.isRequired
  };

  // TODO: fix scrolling to the bottom
  scrollToBottom = () => {
    animateScroll.scrollToBottom({
      containerId: "chat-layout"
    });
  };

  render() {
    this.scrollToBottom();
    const { classes, messages, activeUser, deleteMessage } = this.props;
    return messages && messages.length ? (
      <div
        id="chat-layout"
        className={classes.messagesWrapper}
        ref={wrapper => {
          this.messagesWrapper = wrapper;
        }}
      >
        <Scrollbars
          autoHide
          autoHideTimeout={1000}
          ref={node => {
            this.node = node;
          }}
        >
          {messages.map(message => (
            <ChatMessage
              key={message.id}
              activeUser={activeUser}
              message={message}
              deleteMessage={deleteMessage}
            />
          ))}
        </Scrollbars>
      </div>
    ) : (
      <Typography variant="display1">There is no messages yet...</Typography>
    );
  }
}

export default withStyles(styles)(ChatMessageList);
