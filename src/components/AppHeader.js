import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";
import Avatar from "./CustomAvatar";

const styles = theme => ({
  appBar: {
    position: "fixed",
    width: "100%",
    marginLeft: 320,
    backgroundColor: "#ffffff"
  },
  appBarTitle: {
    flex: 1,
    marginLeft: theme.spacing.unit * 2,
    marginRight: theme.spacing.unit * 2,
    color: "#1d80f6"
  }
});

const AppHeader = ({ classes }) => (
  <AppBar color="primary" className={classes.appBar}>
    <Toolbar color="contrast">
      <React.Fragment>
        <Avatar colorFrom="1">CHAT</Avatar>
        <Typography variant="title" className={classes.appBarTitle}>
          CHAT
        </Typography>
      </React.Fragment>
    </Toolbar>
  </AppBar>
);

AppHeader.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired
};

export default withStyles(styles)(AppHeader);
