/* eslint no-underscore-dangle: 0 */
import React from "react";
import PropTypes from "prop-types";
import classNames from "classnames";
import moment from "moment";
import { withStyles } from "material-ui/styles";
import { Paper } from "material-ui";
import Typography from "material-ui/Typography";
import CustomAvatar from "./CustomAvatar";

import Likes from "./Likes";
import EditButton from "./EditButton";
import DeleteButton from "./DeleteButton";

const styles = theme => ({
  messageWrapper: {
    display: "flex",
    justifyContent: "flex-start",
    alignItems: "center",
    padding: `${theme.spacing.unit}px ${theme.spacing.unit * 3}px`
  },
  messageWrapperFromMe: {
    justifyContent: "flex-end"
  },
  message: {
    maxWidth: "45%",
    minWidth: "10%",
    padding: theme.spacing.unit,
    backgroundColor: "#f2f2f2",
    marginLeft: theme.spacing.unit * 2
  },
  messageFromMe: {
    marginRight: theme.spacing.unit * 2,
    backgroundColor: "#d2f0ff"
  },
  statusMessage: {
    width: "100%",
    textAlign: "center"
  },
  statusMessageUser: {
    display: "inline"
  }
});

class ChatMessage extends React.Component {
  state = {
    isEditing: "false"
  };
  saveEditedMessage = () => {
    // TODO: fix message editing
  };
  renderUserAvatar = avatar => <CustomAvatar imageUrl={avatar} />;
  renderMessageControls = (messageId, deleteMessage) => (
    <div style={{ display: "flex" }}>
      {/* TODO: fix message editing */}
      {/*<EditButton*/}
      {/*  editMessage={() => this.setState({*/}
      {/*    isEditing: 'true',*/}
      {/*  })}*/}
      {/*  messageId={messageId}*/}
      {/*/>*/}
      <DeleteButton messageId={messageId} deleteMessage={deleteMessage} />
    </div>
  );
  renderMessageEditing = () => (
    <div>
      <textarea defaultValue="Edit me" />
      <button onClick={this.saveEditedMessage}>Save</button>
    </div>
  );
  renderMessageNormal = (classes, message, isMessageFromMe, displayedName) => (
    <Paper
      className={classNames(
        classes.message,
        isMessageFromMe && classes.messageFromMe
      )}
    >
      <Typography variant="caption">{displayedName}</Typography>
      <Typography variant="body1">{message.text}</Typography>
      <Typography variant="caption" className={classes.time}>
        {moment(message.createdAt).fromNow()}
      </Typography>
    </Paper>
  );

  render() {
    const { classes, message, activeUser, deleteMessage } = this.props;
    const { isEditing } = this.state;
    const isMessageFromMe = message.userId === activeUser.id;
    const displayedName = isMessageFromMe ? activeUser.name : message.user;
    return (
      <div
        className={classNames(
          classes.messageWrapper,
          isMessageFromMe && classes.messageWrapperFromMe
        )}
      >
        {!isMessageFromMe && this.renderUserAvatar(message.avatar)}
        {isMessageFromMe &&
          this.renderMessageControls(message.id, deleteMessage)}
        {!isEditing
          ? this.renderMessageEditing()
          : this.renderMessageNormal(
              classes,
              message,
              isMessageFromMe,
              displayedName
            )}
        {!isMessageFromMe && <Likes />}
      </div>
    );
  }
}

ChatMessage.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  message: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      avatar: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      editedAt: PropTypes.string.isRequired
    })
  ).isRequired,
  activeUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  deleteMessage: PropTypes.func.isRequired
};

export default withStyles(styles)(ChatMessage);
