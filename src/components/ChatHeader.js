import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import AppBar from "material-ui/AppBar";
import Toolbar from "material-ui/Toolbar";
import Typography from "material-ui/Typography";

const styles = theme => ({
  chatBar: {
    position: "absolute",
    width: "100%",
    marginTop: 75,
    fontWeight: 300,
    fontSize: 20,
    boxShadow: "none",
    backgroundColor: "#1d80f6"
  },
  chatBarTitle: {
    flex: 2,
    marginLeft: theme.spacing.unit,
    color: "#fff"
  },
  chatBarParticipants: {
    flex: 1,
    textAlign: "center",
    color: "#fff"
  },
  chatBarCounter: {
    flex: 1,
    textAlign: "center",
    color: "#fff"
  },
  chatBarTime: {
    flex: 1,
    textAlign: "center",
    color: "#fff"
  }
});

const ChatHeader = ({ name, participants, messages, time, classes }) => (
  <AppBar className={classes.chatBar}>
    <Toolbar>
      <React.Fragment>
        <Typography variant="title" className={classes.chatBarTitle}>
          {name}
        </Typography>
        <Typography
          variant="subheading"
          className={classes.chatBarParticipants}
        >
          {participants} participants
        </Typography>
        <Typography variant="subheading" className={classes.chatBarCounter}>
          {messages} messages
        </Typography>
        <Typography variant="subheading" className={classes.chatBarTime}>
          last message at {time}
        </Typography>
      </React.Fragment>
    </Toolbar>
  </AppBar>
);

ChatHeader.propTypes = {
  name: PropTypes.objectOf(PropTypes.string).isRequired,
  participants: PropTypes.objectOf(PropTypes.number).isRequired,
  messages: PropTypes.objectOf(PropTypes.number).isRequired,
  time: PropTypes.objectOf(PropTypes.string).isRequired,
  classes: PropTypes.objectOf(PropTypes.string).isRequired
};

export default withStyles(styles)(ChatHeader);
