import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import Paper from "material-ui/Paper";
import Input from "material-ui/Input";

const styles = theme => ({
  messageInputWrapper: {
    position: "absolute",
    left: "auto",
    right: 0,
    bottom: 0,
    width: "100%",
    paddingTop: "24px 0"
  },
  messageInput: {
    boxShadow: "none",
    padding: theme.spacing.unit * 2
  }
});

class MessageInput extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    sendMessage: PropTypes.func.isRequired
  };
  constructor() {
    super();
    this.sendMessage = this.sendMessage.bind(this);
  }

  state = {
    value: ""
  };

  sendMessage() {
    this.props.sendMessage(this.state.value);
  }

  handleValueChange = event => {
    this.setState({
      value: event.target.value
    });
  };

  handleKeyPress = event => {
    const { value } = this.state;

    if (event.key === "Enter" && value) {
      this.props.sendMessage(value);
      this.setState({ value: "" });
    }
  };

  render() {
    const { classes } = this.props;

    return (
      <div className={classes.messageInputWrapper}>
        <Paper className={classes.messageInput} elevation={6}>
          <Input
            fullWidth
            placeholder="Type your message and press ENTER …"
            disabled={false}
            value={this.state.value}
            onChange={this.handleValueChange}
            onKeyPress={this.handleKeyPress}
          />
        </Paper>
      </div>
    );
  }
}

export default withStyles(styles)(MessageInput);
