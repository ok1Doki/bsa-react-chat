import React from "react";
import PropTypes from "prop-types";
import Avatar from "material-ui/Avatar";
import { withStyles } from "material-ui/styles";

const styles = theme => ({
  avatarWrapper: {
    display: "inline-block"
  },
  avatarImage: {
    height: "35px",
    width: "35px",
    borderRadius: "50%",
    display: "inline-block",
    margin: "0 10px -10px"
  },
  paper: {
    padding: theme.spacing.unit * 3
  }
});

const CustomAvatar = ({ imageUrl }) => (
  <Avatar className="avatarWrapper" src={imageUrl} />
);

CustomAvatar.propTypes = {
  imageUrl: PropTypes.string
};

CustomAvatar.defaultProps = {
  imageUrl: ""
};

export default withStyles(styles)(CustomAvatar);
