/* eslint-disable prefer-const */
// eslint-disable react/no-unused-state
import React, { Component } from "react";
import fetch from "isomorphic-fetch";
import * as uuid from "uuid";
import { animateScroll } from "react-scroll";
import Loader from "react-loader-spinner";
import PropTypes from "prop-types";
import Chat from "./Chat";
import ChatHeader from "./ChatHeader";
import ErrorMessage from "./ErrorMessage";
import AppHeader from "./AppHeader";

class ChatPage extends Component {
  static propTypes = {
    // eslint-disable-next-line react/no-unused-prop-types
    messages: PropTypes.arrayOf(
      PropTypes.shape({
        id: PropTypes.string.isRequired,
        userId: PropTypes.string.isRequired,
        avatar: PropTypes.string.isRequired,
        user: PropTypes.string.isRequired,
        text: PropTypes.string.isRequired,
        createdAt: PropTypes.string.isRequired,
        editedAt: PropTypes.string.isRequired
      })
    ).isRequired,
    error: PropTypes.instanceOf(Error)
  };
  static defaultProps = {
    error: null
  };

  constructor() {
    super();
    this.sendNewMessage = this.sendNewMessage.bind(this);
    this.deleteMessage = this.deleteMessage.bind(this);
  }

  state = {
    messages: null,
    activeUser: {
      id: 0,
      name: "Nazar Panasiuk"
    }
  };

  componentDidMount() {
    const url = "https://edikdolynskyi.github.io/react_sources/messages.json";

    setTimeout(() => {
      fetch(url)
        .then(response => response.json())
        .then(jsonData => {
          this.setState({
            messages: jsonData.map(item => ({
              id: item.id,
              userId: item.userId,
              avatar: item.avatar,
              user: item.user,
              text: item.text,
              createdAt: item.createdAt,
              editedAt: item.editedAt
            }))
          });
        })
        // eslint-disable-next-line no-console
        .catch(error => console.log(error));
    }, 2000);
  }

  scrollToBottoms = () => {
    animateScroll.scrollToBottom({
      duration: 800,
      delay: 0,
      smooth: "easeInOutQuart"
    });
  };

  sendNewMessage = newMessage => {
    this.scrollToBottoms();
    let newCtcList = [...this.state.messages];
    newCtcList.push({
      id: uuid.v4(),
      userId: this.state.activeUser.id,
      user: this.state.activeUser.name,
      text: newMessage,
      avatar: "",
      createdAt: new Date(),
      editedAt: null
    });
    this.setState({ messages: newCtcList }, () => {});
  };

  editMessage = (messageId, messageText) => {
    this.state.messages.filter(
      message => message.id === messageId
    ).text = messageText;
    this.state.messages.filter(
      message => message.id === messageId
    ).editedAt = new Date();
    this.setState({ messages: this.messages });
  };
  deleteMessage = messageId => {
    const newMessages = this.state.messages.filter(
      message => message.id !== messageId
    );
    this.setState({ messages: newMessages });
  };

  groupBy = function(xs, key) {
    return xs.reduce((rv, x) => {
      // eslint-disable-next-line no-param-reassign
      (rv[x[key]] = rv[x[key]] || []).push(x);
      return rv;
    }, {});
  };

  // eslint-disable-next-line consistent-return
  render() {
    const { messages, activeUser } = this.state;
    const { error } = this.props;
    if (error) {
      return <div>Error: {error.message}</div>;
    }
    if (Array.isArray(messages)) {
      const lastMessageTimeString = messages[messages.length - 1].createdAt;
      const lastMessageTime = new Date(
        lastMessageTimeString
      ).toLocaleTimeString();
      const participantsNum = Object.entries(this.groupBy(messages, "userId"))
        .length;
      const messagesNum = messages.length;
      return (
        <React.Fragment>
          <AppHeader />
          <ChatHeader
            name="Issue #415"
            participants={participantsNum}
            messages={messagesNum}
            time={lastMessageTime}
          />
          <Chat
            messages={messages}
            activeUser={activeUser}
            sendMessage={this.sendNewMessage}
            deleteMessage={this.deleteMessage}
          />
          <ErrorMessage error={error} />
        </React.Fragment>
      );
    }
    return (
      <React.Fragment>
        <div style={{ margin: "0 auto" }}>
          <Loader type="ThreeDots" color="#4939f5" height={100} width={100} />
        </div>
      </React.Fragment>
    );
  }
}

export default ChatPage;
