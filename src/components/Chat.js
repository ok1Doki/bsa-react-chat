import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "material-ui/styles";
import ChatMessageList from "./ChatMessageList";
import MessageInput from "./MessageInput";

const styles = () => ({
  chatLayout: {
    display: "flex",
    justifyContent: "center",
    paddingTop: "125px",
    height: "100%",
    width: "100%",
    paddingBottom: "40px",
    backgroundColor: "#fff"
  }
});

const Chat = ({
  classes,
  messages,
  activeUser,
  sendMessage,
  deleteMessage,
  editMessage
}) => (
  <main className={classes.chatLayout}>
    <ChatMessageList
      messages={messages}
      activeUser={activeUser}
      editMessage={editMessage}
      deleteMessage={deleteMessage}
    />
    <MessageInput sendMessage={sendMessage} activeUser={activeUser} />
  </main>
);

Chat.propTypes = {
  classes: PropTypes.objectOf(PropTypes.string).isRequired,
  messages: PropTypes.arrayOf(
    PropTypes.shape({
      id: PropTypes.string.isRequired,
      userId: PropTypes.string.isRequired,
      avatar: PropTypes.string.isRequired,
      user: PropTypes.string.isRequired,
      text: PropTypes.string.isRequired,
      createdAt: PropTypes.string.isRequired,
      editedAt: PropTypes.string.isRequired
    })
  ).isRequired,
  activeUser: PropTypes.shape({
    id: PropTypes.string.isRequired,
    name: PropTypes.string.isRequired
  }).isRequired,
  sendMessage: PropTypes.func.isRequired,
  editMessage: PropTypes.func.isRequired,
  deleteMessage: PropTypes.func.isRequired
};

export default withStyles(styles)(Chat);
