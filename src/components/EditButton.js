import * as React from "react";
import { IconButton } from "material-ui";
import { Edit } from "material-ui-icons";
import { withStyles } from "material-ui/styles";
import PropTypes from "prop-types";

const styles = () => ({
  button: {
    color: "grey",
    fontSize: "16px",
    margin: "4px",
    width: "auto"
  }
});

class EditButton extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired,
    messageId: PropTypes.objectOf(PropTypes.string).isRequired,
    editMessage: PropTypes.func.isRequired
  };

  constructor() {
    super();
    this.editMessage = this.editMessage.bind(this);
  }

  editMessage() {
    this.props.editMessage(this.props.messageId);
  }
  render() {
    const { classes } = this.props;

    return (
      <div>
        <IconButton
          className={classes.button}
          onClick={this.editMessage}
          component="span"
        >
          <Edit />
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)(EditButton);
