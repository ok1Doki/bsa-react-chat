import * as React from "react";
import { IconButton } from "material-ui";
import { Favorite } from "material-ui-icons";
import { withStyles } from "material-ui/styles";
import PropTypes from "prop-types";

const styles = () => ({
  likeCounterCaption: {
    fontSize: "12px",
    margin: "4px"
  },
  likedButton: {
    color: "red",
    fontSize: "16px",
    margin: "4px"
  },
  unlikedButton: {
    color: "grey",
    fontSize: "16px",
    margin: "4px"
  }
});

class Likes extends React.Component {
  static propTypes = {
    classes: PropTypes.objectOf(PropTypes.string).isRequired
  };

  constructor(props) {
    super(props);
    this.state = {
      likes: Math.floor(Math.random() * 100),
      updated: false
    };
  }

  updateLikes = () => {
    if (!this.state.updated) {
      this.setState(prevState => ({
        likes: prevState.likes + 1,
        updated: true
      }));
    } else {
      this.setState(prevState => ({
        likes: prevState.likes - 1,
        updated: false
      }));
    }
  };

  render() {
    const { classes } = this.props;
    const { updated } = this.state;

    return (
      <div>
        <IconButton
          className={updated ? classes.likedButton : classes.unlikedButton}
          onClick={this.updateLikes}
          aria-label="like message"
          component="span"
        >
          <Favorite />
          <p className={classes.likeCounterCaption}>{this.state.likes}</p>
        </IconButton>
      </div>
    );
  }
}

export default withStyles(styles)(Likes);
